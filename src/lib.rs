//! A simple crate to run powerband analysis. Useful to answer questions like:
//! "What is the average amplitide of signal components in a given frequency
//! range in my sampled data?"
//! 
//! We use a ring buffer to store the window so we can add samples with O(1)
//! time. `rustfft` is used internally to do frequency analysis but only when
//! a sample statistic is requested.
//! 
//! # Examples:
//! ```
//! use bandpower_rt::SignalWindow;
//! 
//! // Generate a new window for 1000 samples with a 1000Hz sample frequency
//! let mut signal_window = SignalWindow::with_capacity(1000, 0.001);
//! 
//! // Populate the window
//! for sample_num in 0..1000 {
//!     let sample_num = sample_num as f64;
//!     signal_window.push((sample_num*0.001*100.0*2.0*3.1415).sin()+(sample_num*0.001*10.0*2.0*3.1415).sin()*3.0);
//! }
//! 
//! // Calculate the summary statistic
//! let rms_in_band = signal_window.calculate_average_band_amplitude(&[0.0, 25.0]).unwrap();
//! print!("The RMS amplitude of signal components in the (0, 25 Hz) range is: {}", rms_in_band);
//! ```

use std::{collections::VecDeque, ops::Div};
use rustfft::FftPlanner;

/// Usage as follows:
/// ```
/// use bandpower_rt::SignalWindow;
/// 
/// // Generate a new window for 1000 samples with a 1000Hz sample frequency
/// let mut signal_window = SignalWindow::with_capacity(1000, 0.001);
/// 
/// // Populate the window
/// for sample_num in 0..1000 {
///     let sample_num = sample_num as f64;
///     signal_window.push((sample_num*0.001*100.0*2.0*3.1415).sin()+(sample_num*0.001*10.0*2.0*3.1415).sin()*3.0);
/// }
/// 
/// // Calculate the summary statistic
/// let rms_in_band = signal_window.calculate_average_band_amplitude(&[0.0, 25.0]).unwrap();
/// print!("The RMS amplitude of signal components in the (0, 25 Hz) range is: {}", rms_in_band);
/// ```
/// 
pub struct SignalWindow {
    samples : VecDeque<f64>, 
    capacity : usize,
    sampling_freq : f64,
    fft_planner: FftPlanner<f64>,
}

#[derive(Debug)]
pub enum SignalError {
    InvalidFrequencyRange,
    InvalidFrequencyRequest,
}

impl SignalWindow {
    /// Generate a new [`SignalWindow`] with a known capacity and sampling period.
    /// Note: This will reserve memory for the window and initialize the window
    /// with 0.0 to start.
    pub fn with_capacity(capacity : usize, sampling_period: f64) -> SignalWindow {
        let mut window = SignalWindow { 
            samples: VecDeque::with_capacity(capacity), 
            capacity ,
            sampling_freq: 1.0/sampling_period,
            fft_planner: FftPlanner::new()
        };
        window.samples.resize_with(capacity, || {
            0.0
        });
        window
    }

    /// Restores the internal sample buffer to 0.0, handy for when you need to 
    /// reuse an instance of SignalWindow.
    pub fn reset(&mut self) {
        self.samples.iter_mut().for_each(|elem| {
            *elem = 0.0;
        })
    }

    /// Add an element to the internal ring-buffer. Also removes the least recently
    /// added element. 
    pub fn push(&mut self, elem : f64) {
        self.samples.pop_front();
        self.samples.push_back(elem);
    }

    /// Calculates the RMS amplitude of the dataset.
    pub fn calculate_rms(&self) -> f64 {
        self.samples.iter().fold(0.0, |b, elem| {
            b + elem.powf(2.0)
        }).div(self.capacity as f64).sqrt()
    }

    /// Calculates the average amplitude inside a given frequency band. Will error if freq_range is not
    /// strictly less than half the sampling freuqency or if freq_range is not monontonically increasing.
    pub fn calculate_average_band_amplitude(&mut self, freq_range: &[f64; 2]) -> Result<f64, SignalError> {
        if freq_range[1] < freq_range[0] {
            return Err(SignalError::InvalidFrequencyRange);
        } else if freq_range[1] > self.sampling_freq/2.0 {
            return Err(SignalError::InvalidFrequencyRequest);
        }
        let fft = self.fft_planner.plan_fft(self.capacity, rustfft::FftDirection::Forward);
        let mean = self.samples.iter().fold(0.0, |b, elem| {
            b + elem/self.capacity as f64
        });
        let mut buffer: Vec<rustfft::num_complex::Complex<f64>> = self.samples.iter().map(|elem| {
            (elem - mean).into()
        }).collect();
        fft.process(&mut buffer);
        
        let min_bound = (freq_range[0]/self.sampling_freq*self.capacity as f64).floor() as usize;
        let max_bound = (freq_range[1]/self.sampling_freq*self.capacity as f64).ceil() as usize;

        let freq_spacing = self.sampling_freq/self.capacity as f64;

        let psd : Vec<f64> = buffer.iter().map(|elem| {
            2.0*(elem.conj()*elem).re/(self.sampling_freq*self.capacity as f64)
        }).collect();

        let bandpower = psd[min_bound..max_bound].iter().fold(0.0, |b, elem| {
            b + elem*freq_spacing
        });

        Ok(bandpower.sqrt())
    } 


}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn basic_functionality() {
        // Generate a new window for 1000 samples with a 1000Hz sample frequency
        let mut signal_window = SignalWindow::with_capacity(1000, 0.002);

        // Populate the window
        for sample_num in 0..1000 {
            let sample_num = sample_num as f64;
            signal_window.push((sample_num*0.002*100.0*2.0*3.1415).sin()+(sample_num*0.002*10.0*2.0*3.1415).sin()*6.0);
        }

        // Calculate the summary statistic
        let rms_in_band = signal_window.calculate_average_band_amplitude(&[0.0, 25.0]).unwrap();
        let total_rms = signal_window.calculate_rms();
        println!("The RMS amplitude of signal components in the (0, 25 Hz) range is: {}. Total RMS: {}", rms_in_band, total_rms);
    }

    #[test]
    fn basic_functionality_2() {
        // Generate a new window for 1000 samples with a 1000Hz sample frequency
        let mut signal_window = SignalWindow::with_capacity(5000, 0.0005);

        // Populate the window
        for sample_num in 0..5000 {
            let sample_num = sample_num as f64;
            signal_window.push((sample_num*0.0005*100.0*2.0*3.1415).sin()+(sample_num*0.0005*10.0*2.0*3.1415).sin()*6.0);
        }

        // Calculate the summary statistic
        let rms_in_band = signal_window.calculate_average_band_amplitude(&[0.0, 25.0]).unwrap();
        println!("The RMS amplitude of signal components in the (0, 25 Hz) range is: {}", rms_in_band);
    }
}
