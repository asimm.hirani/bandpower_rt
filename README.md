# bandpower_rt

A simple crate to run powerband analysis. Useful to answer questions like:
"What is the average amplitide of signal components in a given frequency
range in my sampled data?"

We use a ring buffer to store the window so we can add samples with O(1)
time. `rustfft` is used internally to do frequency analysis but only when
a sample statistic is requested.

## Examples:
```rust
use bandpower_rt::SignalWindow;

// Generate a new window for 1000 samples with a 1000Hz sample frequency
let mut signal_window = SignalWindow::with_capacity(1000, 0.001);

// Populate the window
for sample_num in 0..1000 {
    let sample_num = sample_num as f64;
    signal_window.push((sample_num*0.001*100.0*2.0*3.1415).sin()+(sample_num*0.001*10.0*2.0*3.1415).sin()*3.0);
}

// Calculate the summary statistic
let rms_in_band = signal_window.calculate_average_band_amplitude(&[0.0, 25.0]).unwrap();
print!("The RMS amplitude of signal components in the (0, 25 Hz) range is: {}", rms_in_band);
```
